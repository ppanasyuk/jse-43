package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.model.ITaskRepository;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull Class<Task> getEntityClass() {
        return Task.class;
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.%s.%s = :%s AND m.%s.%s = :%s ORDER BY m.%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_PROJECT,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_PROJECT_ID,
                FieldConst.FIELD_CREATED
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .setParameter(FieldConst.FIELD_PROJECT_ID, projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull String userId, @Nullable String projectId) {
        @NotNull final String jpql = String.format(
                "DELETE FROM %s m WHERE m.%s.%s = :%s AND m.%s.%s = :%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_PROJECT,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_PROJECT_ID
        );
        entityManager
                .createQuery(jpql)
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .setParameter(FieldConst.FIELD_PROJECT_ID, projectId)
                .executeUpdate();
    }

}