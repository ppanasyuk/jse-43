package ru.t1.panasyuk.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;

public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<UserDTO> getEntityClass() {
        return UserDTO.class;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null) return null;
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.%s = :%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_LOGIN,
                FieldConst.FIELD_LOGIN
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setParameter(FieldConst.FIELD_LOGIN, login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null) return null;
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.%s = :%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_EMAIL,
                FieldConst.FIELD_EMAIL
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setParameter(FieldConst.FIELD_EMAIL, email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

}